﻿using Newtonsoft.Json.Linq;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomationTest
{
    [TestFixture]
    public class SearchTest
    {
        JArray data = null;
        Config configData = null;
        IWebDriver chrome;
        [SetUp]
        public void Intialize()
        {
            System.IO.Directory.SetCurrentDirectory(System.AppDomain.CurrentDomain.BaseDirectory);

            data = DataHelper.GetData(@"Data\input.json");
            configData = DataHelper.GetConfigration(@"Data\config.json");

            ChromeOptions options = new ChromeOptions();
            options.AddArguments("--lang=en");

            chrome = new ChromeDriver(configData.SelenioumDriver, options, TimeSpan.FromMinutes(2));
            chrome.Url = configData.URL;
            //wait to load page
            System.Threading.Thread.Sleep(1000);
        }

        [TearDown]
        public void End()
        {
            chrome.Close();
            chrome.Quit();
        }

        [Test]
        public void TestSearchSuccess()
        {
            bool isSucess = false;
            try
            {
                var searchInput = chrome.FindElement(By.XPath("/html/body/div[1]/div[3]/form/div[1]/div[1]/div[1]/div/div[2]/input"));
                if (searchInput != null)
                {
                    searchInput.SendKeys(data[0]["keyword"].ToString());
                    searchInput.SendKeys(Keys.Enter);
                    System.Threading.Thread.Sleep(1000);
                    var searchPagesRow = chrome.FindElement(By.ClassName("AaVjTc"));
                    if(searchPagesRow != null)
                    {
                        var pages = searchPagesRow.FindElements(By.TagName("td"));
                        if(pages != null && pages.Count > 6)
                        {
                            if(pages.Count == 8)
                            pages[4].Click();
                            else
                                pages[3].Click();
                            System.Threading.Thread.Sleep(1000);
                            var searchResult = chrome.FindElements(By.ClassName("g"));
                            if (searchResult != null && searchResult.Count > 0)
                            {
                                var beinSport = searchResult[0].FindElement(By.TagName("a"));
                                if(beinSport != null && beinSport.Text.Contains("beinsports"))
                                {
                                    beinSport.Click();
                                    System.Threading.Thread.Sleep(1000);
                                    var socialMediaLinks = chrome.FindElement(By.ClassName("top_bar__list"));
                                    if(socialMediaLinks != null)
                                    {
                                        var linksList = socialMediaLinks.FindElements(By.TagName("li"));
                                        if(linksList != null && linksList.Count == 4)
                                        {
                                            foreach(var link in linksList)
                                            {
                                                link.Click();
                                            }

                                            isSucess = true;
                                        }
                                    }
                                    
                                    
                                }

                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
               
            }

            Assert.IsTrue(isSucess);
        }
    }
}
