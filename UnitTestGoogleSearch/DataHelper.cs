﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomationTest
{
    public class DataHelper
    {
        public static JArray GetData(string fileName)
        {
            return (JArray)JsonConvert.DeserializeObject(File.ReadAllText(fileName));
        }

        public static Config GetConfigration(string fileName)
        {
            try
            {
               var obj = (JObject)JsonConvert.DeserializeObject(File.ReadAllText(fileName));
                if (obj != null)
                    return new Config() {  SelenioumDriver = obj["selenioumDriver"].ToString() , URL = obj["URL"].ToString() };
            }
            catch (Exception)
            {
 
            }
            return null;
        }
    }

    public class Config
    {
        public string SelenioumDriver { get; set; }
        public string URL { get; set; }
    }
}
